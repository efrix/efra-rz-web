# Start project

## v1.0.2

Completar los siguientes puntos para llegar a la versión v.1.0.2

---

### Refactorización de archivos Pug
- [x]  Implementar estructura de EDgrid Styleguides en [Github](https://github.com/escueladigital/EDteam-StyleGuides/tree/master/dev)
- [x]  Agregar a .gitignore los HTML de la raíz del proyecto y carpeta CSS

### Crear página de vista previa del proyecto (Rama project)
- [x]  Establecer estilos de página
- [ ]  Mostrar boton siguiente anterior(Se pasa para implementar en  version posterior)
- [x]  Logotipos de tecnologías en svg
- [x]  Botón para ver el proyecto
- [x]  Botón para ir a contacto
- [x]  Imagen de captura del proyecto

### Crear páginas de proyectos (Rama project)
- [x]  Senssthetic
- [x]  Diamond
- [x]  Arxav
- [x]  Sanimack
- [x]  Cirugía
- [x]  Gensteel
- [x]  Karla velver
- [x]  Gema
- [x]  Odontología

### Establecer Menú en Móviles (Rama movilMenu)
- [X] Menu amburguesa para dispositivos móviles
- [X] Implementar codigo de ejemplo de FidelTamezSalinas https://codepen.io/FTamez/pen/JLQdqX?editors=0100

Una línea creada en windows.